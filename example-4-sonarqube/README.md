# course-jenkins

**Sonarqube**

เป็น Open Source ที่พัฒนาขึ้น เพื่อช่วยตรวจสอบคุณภาพของโค้ด (Code Quality) ที่เราเขียนขึ้นมา 

โดยจะโดยทำการวิเคราะห์โค้ดแบบอัตโนมัติ เพื่อตรวจหา bug, code smell และ ช่องโหว่ความปลอดภัย


**Lab**

1. ทดลองสร้าง Pipeline เพื่อ download sourcecode
 > ```https://gitlab.com/isnoopy/git-lab-workshop```
2. สั่งให้ตรวจสอบคุณภาพโค๊ด

**Pre-require**
- Sonarqube Server
- Jenkin with `SonarQube Scanner` Plugin


**Official Link** 
- https://www.sonarqube.org/